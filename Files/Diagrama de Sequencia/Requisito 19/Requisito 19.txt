@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Jogador envia escolhe qual time ele quer participar.
Cliente -> Cliente: Prepara o comando: team < iDJogador > < team > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: team < iDJogador > < team > em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Coloca o usu�rio no time que ele escolheu.
Cliente -> Jogador : Notifica no batepapo o time que o usu�rio est� participando .

@enduml
