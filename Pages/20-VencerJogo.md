﻿
#  Requisito 20 - Vencer o Jogo
## 1 - Resumo
**História de Usuário**
>Como o jogador, gostaria de vencer o jogo.

**Pré-condições**
>Jogo deverá iniciar com sucesso.

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O jogador não alcança o topo com as peças do jogo.<br/>
    1.2. Demais Jogadores perdem a partida.<br/>

>2. **Fluxo Alternativo** <br/>
    2.1. Caso alguma peça atinja o topo, então o jogador perde;<br/>


**Observações**
>Os jogadores podem sair da sala desistindo do jogo ou perdendo conexão na rede
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerwon < iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que ganhou o jogo;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>706c61796572776f6e2032<br/>
> --------- em **Texto imprimivel**: playerwon 2<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerwon < iDJogador >ÿ winlist  List{< type >< nickname >;< points >}ÿ**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador que ganhou o jogo;<br/>
> --------- type:char => Qual o tipo do vencedor (t = Team ou p = Player)<br/>
> --------- nickname:string => Nickname do Jogador/Time<br/>
> --------- points:number => Pontuação do Jogador/Time <br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: <br/>45c12940407b307ae1eb742b2ed3a374d25ffeadfef444edfef444eff706c61796572776f6e2032ff77696e6c6973742020706774657472696e6574343b3720706774657472696e6574353233343b3620706774657472696e6574353b3420706774657472696e6574333b332074686168613b3320706774657472696e657435333b32ff<br/>
> --------- em **Texto imprimivel**: playerwon 2ÿ winlist  pgtetrinet4;7 pgtetrinet5234;6 pgtetrinet5;4 pgtetrinet3;3 thaha;3 pgtetrinet53;2ÿ<br/>
## 3 - Diagrama de sequência

![Requisito 20](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2020/Requisito%2020%20-%20Vencer%20Jogo.png)



